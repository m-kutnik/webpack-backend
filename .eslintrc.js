module.exports = {
  root: true,
  parser: 'babel-eslint',
  env: {
    browser: true,
    node: true
  },
  extends: 'airbnb-base',
  // add your custom rules here
  rules: {
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    'no-console': process.env.NODE_ENV === 'production' ? 2 : 0,
    'no-trailing-spaces': ["error", { "skipBlankLines": true }],
    'comma-dangle': ['error', 'always-multiline'],
    'arrow-parens': ["error", "always"],
    'max-len': 0,
    'quotes': 0,
    'semi': 0
  },
  globals: {}
}
